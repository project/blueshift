<?php

/**
 * @file
 * Implements Blueshfit as a Drupal MailSystemInterface
 */

/**
 * Modify the drupal mail system to use Blueshift when sending emails.
 */
class BlueshiftMailSystem implements MailSystemInterface {

  /**
   * Format the message for the receiver.
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return array
   *   The formatted $message.
   */
  public function format(array $message) {
    // Leave the message be. @todo does any formatting needs to happen?
    return $message;
  }

  /**
   * Send the email message.
   *
   * @see drupal_mail()
   *
   * @param array $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return bool
   *   TRUE if the mail was successfully accepted, otherwise FALSE.
   */
  public function mail(array $message) {

    if (!isset($message['blueshift'])) {
      watchdog('blueshift', "BlueshiftMailSystem received mail to send but could not be sent because message was missing required 'blueshift' element", array(), WATCHDOG_NOTICE);
      return FALSE;
    }
    elseif (!isset($message['blueshift']['event']) && !isset($message['blueshift']['campaign_uuid'])) {
      watchdog('blueshift', "BlueshiftMailSystem received mail to send but could not be sent because message contained no 'event' or 'campaign_uuid' elements.", array(), WATCHDOG_NOTICE);
      return FALSE;
    }
    $event_api_key = variable_get('blueshift_event_api_key', '');
    $user_api_key = variable_get('blueshift_user_api_key', '');
    if (empty($event_api_key) || empty($user_api_key)) {
      watchdog('blueshift', "Blueshift API key must be set.", array(), WATCHDOG_ERROR);
      return FALSE;
    }

    $config = array();
    // Allow alteration of message configuration in context before sending on.
    drupal_alter('blueshift', $message, $config);
    $blueshift_client = new Blueshift($event_api_key, $user_api_key, $config);

    $blueshift_message = $message['blueshift'];
    try {
      $response = $this->sendWithRetries($blueshift_message, $blueshift_client);

      if (isset($response[Blueshift::RESPONSE_KEY_STATUS]) && $response[Blueshift::RESPONSE_KEY_STATUS] === Blueshift::RESPONSE_OK || !empty($response[Blueshift::RESPONSE_KEY_SUCCESS])) {
        module_invoke_all('blueshift_response_ok', $blueshift_message, $response);
        return TRUE;
      }

      module_invoke_all('blueshift_response_error', $blueshift_message);
      watchdog('blueshift', 'Error in Blueshift response @response', ['@response' => json_encode($response)], WATCHDOG_ERROR);
      return FALSE;
    }
    catch (Exception $e) {
      watchdog_exception('blueshift', $e, 'Exhausted retry attempts to post message to Blueshift. Tried to send: @message', ['@message' => json_encode($blueshift_message)], WATCHDOG_ALERT);
      return FALSE;
    }
  }


  /**
   * Send message and retry on temporary errors like empty response.
   *
   * @param array $blueshift_message
   *   Blueshift mail message.
   * @param Blueshift $blueshift_client
   *   Blueshift client.
   * @param int $retry_count
   *   Retry counter.
   *
   * @return array|FALSE
   *   Response array on successful send or FALSE if unsucessfull.
   *
   * @throws Exception
   *   On retry attempts exhausted.
   */
  protected function sendWithRetries(array $blueshift_message, Blueshift $blueshift_client, $retry_count = 0) {
    $attempt_retry = FALSE;
    try {
      if (isset($blueshift_message['campaign_uuid']) && isset($blueshift_message['email'])) {
        // Prevent Blueshift from rejecting emails that are uppercase (it does)
        // and that might contain hanging whitespace.
        $email = strtolower(trim($blueshift_message['email']));
        $response = $blueshift_client->sendCampaign($email, $blueshift_message['campaign_uuid'], $blueshift_message['parameters']);
      }
      else {
        $response = $blueshift_client->track($blueshift_message['event'], $blueshift_message['parameters']);
      }

      // Temporary error responses are an exception in order to retry.
      if (is_array($response) && Blueshift::isTemporaryErrorResponse($response)) {
        $attempt_retry = TRUE;
      }
    }
    catch (Exception $e) {
      watchdog_exception('blueshift', $e);
      $attempt_retry = TRUE;
      $response = [];
    }

    if ($attempt_retry) {
      if ($retry_count < variable_get('blueshift_send_retry_count', 8)) {
        $retry_count++;
        // Delay next request.
        usleep(100 * $retry_count);
        return $this->sendWithRetries($blueshift_message, $blueshift_client, $retry_count);
      }
      throw new Exception('Exhausted retry attempts');
    }

    return $response;
  }

}
