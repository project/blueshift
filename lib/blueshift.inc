<?php

/**
 * @file
 * Blueshift client class.
 */

/**
 * Class Blueshift
 */
class Blueshift {

  const API_ENDPOINT = 'https://api.getblueshift.com/api/v1';
  const API_EVENT = '/event';
  const API_CAMPAIGN_EXECUTE = '/campaigns/execute';
  const API_USER = '/customers';

  const RESPONSE_KEY_SUCCESS = 'success';
  const RESPONSE_KEY_STATUS = 'status';
  const RESPONSE_KEY_ERROR = 'error';

  const RESPONSE_OK = 'ok';

  protected $eventApiKey;
  protected $userApiKey;
  protected $config;

  /**
   * Blueshift client.
   *
   * @param string $event_api_key
   *   Event API key.
   * @param string $user_api_key
   *   User API key.
   * @param array $config
   *   Optional configuration.
   */
  public function __construct($event_api_key, $user_api_key, array $config = array()) {
    $this->eventApiKey = $event_api_key;
    $this->userApiKey = $user_api_key;;

    // Allow configuration to overridden.
    $this->config = array_merge(
      array(
        'user_agent' => 'DrupalBlueshift/' . VERSION,
        'timeout' => 5,
        'api_endpoint' => self::API_ENDPOINT,
      ),
      $config
    );
  }

  /**
   * Check if response is a temporary error.
   *
   * @param array $response
   *   Response from Blueshift.
   *
   * @return bool
   *   If response is a temporary error and request should be retried.
   */
  public static function isTemporaryErrorResponse(array $response) {
    // Body status codes 503, 504, and 409 are considered temporary errors.
    $temporary_error_codes = array('503', '504', '409');
    if (isset($response[self::RESPONSE_KEY_STATUS]) && in_array($response[self::RESPONSE_KEY_STATUS], $temporary_error_codes)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Send event to Blueshift.
   *
   * @see https://docs.getblueshift.com/docs/event-api
   *
   * @param string $event
   *   Event name.
   * @param array $attributes
   *   Attributes to send with event.
   *
   * @return array
   *   JSON decoded response.
   *
   * @throws Exception
   *   Throws exception on empty server response.
   */
  public function track($event, array $attributes = array()) {
    $data = $attributes;
    $data['event'] = $event;
    $result = $this->post(self::API_EVENT, $this->eventApiKey, $data);
    if (empty($result)) {
      throw new Exception('Empty response from server');
    }
    return json_decode($result, TRUE);
  }

  /**
   * Trigger a campaign to send.
   *
   * @see https://docs.getblueshift.com/docs/campaigns-transactional#transactional_send
   *
   * @param string $email
   *   Email address to deliver campaign to.
   * @param string $campaign_uuid
   *   UUID of campaign.
   * @param array $attributes
   *   Attributes (context) to send.
   *
   * @return array
   *   JSON decoded response.
   *
   * @throws Exception
   *   Throws exception on empty server response.
   */
  public function sendCampaign($email, $campaign_uuid, array $attributes = array()) {
    $data = array(
      'email' => $email,
      'campaign_uuid' => $campaign_uuid,
      'context' => $attributes,
    );
    if (isset($attributes['retailer_customer_id'])) {
      $data['retailer_customer_id'] = $attributes['retailer_customer_id'];
    }
    $result = $this->post(self::API_CAMPAIGN_EXECUTE, $this->userApiKey, $data);
    if (empty($result)) {
      throw new Exception('Empty response from server');
    }
    return json_decode($result, TRUE);
  }

  /**
   * Creates or updates a Blueshift customer.
   *
   * @param string $customer_id
   *   The customer ID.
   * @param array $attributes
   *   The customer attributes to send.
   *
   * @return array
   *   JSON decoded response.
   *
   * @throws Exception
   *   Throws exception on empty server response.
   */
  public function updateCustomer($customer_id, array $attributes = array()) {
    $data = array_merge(array(
      'customer_id' => $customer_id,
    ), $attributes);
    $result = $this->post(self::API_USER, $this->userApiKey, $data);
    if (empty($result)) {
      throw new Exception('Empty response from server');
    }
    return json_decode($result, TRUE);
  }

  /**
   * Send event.
   *
   * @param string $path
   *   API path to post to.
   * @param string $api_key
   *   API key for endpoint.
   * @param array $parameters
   *   Parameters to send.
   *
   * @return string
   *   Response.
   */
  protected function post($path, $api_key, array $parameters) {
    $url = $this->config['api_endpoint'] . $path;
    $data = json_encode($parameters);

    $ch = curl_init();
    curl_setopt_array($ch, array(
      CURLOPT_USERPWD => $api_key,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_CONNECTTIMEOUT => $this->config['timeout'],
      CURLOPT_URL => $url,
      CURLOPT_HTTPHEADER => array(
        'Content-Type:application/json',
        'Accept:application/json',
      ),
      CURLOPT_USERAGENT => $this->config['user_agent'],
      CURLOPT_POSTFIELDS => $data,
    ));
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
  }
}
