## Blueshift Drupal mailsystem module

Provides Blueshift integration with Drupal to send site mails to Blueshift.

https://getblueshift.com/

## Dependencies

Mailsystem module https://www.drupal.org/project/mailsystem

## Installation

Enable via Drupal UI or drush. See configuration for required API key variables.

## Configuration

Set Blueshift event API key and user API key to variables
'blueshift_event_api_key' and 'blueshift_user_api_key', respectively.
Preferrably do this in your settings.php or including settings file.

Drupal's mail system can be set to 'BlueshiftMailSystem', read mailsystem 
module's documentation for how to use different mail system plugins.

`variable_set('mail_system', ['default-system' => 'BlueshiftMailSystem']);`
